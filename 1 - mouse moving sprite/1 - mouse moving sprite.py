'''
1.Рисование на экране

[темы]
начальная инициализация pygame
игровой цикл
отрисовка объектов на экране
прием сигналов с клавиатуры, мыши
реакция на эти сигналы

[задания]:
сделать запрет выхода объекта за границы экрана при управлении мышкой
сделать, чтобы объект,управляемый с клавиатуры при выходе за экран появлялся с другой стороны
'''
import pygame

# начальная инициализация
pygame.init()
# создание часов, отслеживающих частоту отрисовки
clock = pygame.time.Clock()
# используемые переменные
# цвета RGB
black = (0, 0, 0)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
grey = (128, 128, 128)
# радиусы кругов
radius1 = 30
radius2 = 25
radius3 = 20
radius4 = 15
# высота и ширина экрана в пикселях
width = 640
height = 480
# центр шаров
x1, y1 = x2, y2 = x3, y3 = x4, y4 = (width // 2, height // 2)

# установка разрешения экрана в пикселях, получение рабочей поверхности Surface
screen = pygame.display.set_mode((width, height))

# начало игрового цикла
game_loop = True
while game_loop:

    # первый вариант управления - получение всех событий, выбор нужных
    for e in pygame.event.get():
        # событие выхода
        if e.type == pygame.QUIT: game_loop = False

        # событие движения мыши
        if e.type == pygame.MOUSEMOTION:
            x1, y1 = e.pos

        # событие нажатия клавишы клавиатуры
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_ESCAPE: game_loop = False
            if e.key == pygame.K_UP:    y2 -= 5
            if e.key == pygame.K_DOWN:  y2 += 5
            if e.key == pygame.K_LEFT:  x2 -= 5
            if e.key == pygame.K_RIGHT: x2 += 5

    # второй вариант управления: получение списка нажатых клавиш
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_w]: y3 -= 10
    if pressed[pygame.K_s]: y3 += 10
    if pressed[pygame.K_a]: x3 -= 10
    if pressed[pygame.K_d]: x3 += 10

    # получение текущей координаты мышки
    coord = pygame.mouse.get_pos()
    x4 = width - coord[0]
    y4 = height - coord[1]

    # закрашиваем всю рабочую поверхность черным
    screen.fill(black)
    # рисуем наши круги
    pygame.draw.circle(screen, red, (x1, y1), radius1)
    pygame.draw.circle(screen, green, (x2, y2), radius2)
    pygame.draw.circle(screen, blue, (x3, y3), radius3)
    pygame.draw.circle(screen, grey, (x4, y4), radius4)
    # установка задержки отрисовки (максимальный FPS)
    clock.tick(60)
    # обновление изображения на экране
    pygame.display.flip()
