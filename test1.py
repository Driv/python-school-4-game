import random
import options
import pygame
import sys
import os


def get_item(item_name, lvl):
    return random.choice(list(filter(lambda x: x[2] == lvl, options.items[item_name])))


class Base:
    def __init__(self, _param1, _param2):
        self.param1 = _param1
        self.param2 = _param2
        self.test = 100


class Base2(Base):
    param3 = 3
    param4 = 4


class Base3(Base2):
    param5 = 5
    param1 = 33
    test = 500
    i = []

    def __str__(self):
        return '[{},{},{},{},{},{}]'.format(self.param1, self.param2, self.param3, self.param4, self.param5, self.test)


a = Base3(11, 22)
# i = sum(p[1] for p in options.items['shield'] if p[3] < 5)
# print(str(i))
'''

x = (2, -3)
y = (0, 0)
print(x > y)


pygame.init()
screen = pygame.display.set_mode((320,240))
myfont = pygame.font.SysFont("monospace", 15)

# render text
label = myfont.render("Some text!", 1, (255,255,0))
screen.blit(label, (100, 100))

while True:
    for e in pygame.event.get():
        if e.type == pygame.QUIT: sys.exit(0)
    pygame.display.flip()

a=Base3(1,2)
b = Base3(3,4)
c = Base3(5,6)
b.i.append([1,2,3])
print(a.i, b.i, c.i)
'''
print(os.listdir('..'))
print(os.getcwd())