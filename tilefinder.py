import pygame
import sys
from pygame import sprite

theClock = pygame.time.Clock()

background = pygame.image.load('tileset.png')
pygame.font.init()
font = pygame.font.SysFont('Times New Roman', 30)

background_size = background.get_size()
background_rect = background.get_rect()
screen = pygame.display.set_mode(background_size, pygame.RESIZABLE)
w, h = background_size



screen.blit(background, background_rect)
pygame.display.flip()
running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT: screen.scroll(32, 0)
            if event.key == pygame.K_RIGHT: screen.scroll(-32, 0)
            if event.key == pygame.K_UP: screen.scroll(0, 32)
            if event.key == pygame.K_DOWN: screen.scroll(0, -32)

        if event.type == pygame.MOUSEMOTION:
            pygame.display.set_caption(
                str(pygame.mouse.get_pos()[0] // 32) + ',' + str(pygame.mouse.get_pos()[1] // 32))
        if event.type == pygame.MOUSEBUTTONDOWN:
            print(str(pygame.mouse.get_pos()[0] // 32) + ',' + str(pygame.mouse.get_pos()[1] // 32))
            text = font.render(str(pygame.mouse.get_pos()[0] // 32) + ',' + str(pygame.mouse.get_pos()[1] // 32), True, (255, 255, 255))
            screen.blit(text, pygame.mouse.get_pos())
            #text_to_screen(screen, str(pygame.mouse.get_pos()[0] // 32) + ',' + str(
            #    pygame.mouse.get_pos()[1] // 32), pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])

        pygame.display.update()
        theClock.tick(60)
