import hero
import random
from options import *

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode(screen_size)

Engine = hero.GameEngine(screen)
for x in range(0, screen_width // sprite_x):
    for y in range(0, screen_height // sprite_y):
        Engine.add_terrain((x, y), random.choice(['grass', 'sand', 'rock', 'water']))

for i in range(0, item_count):
    Engine.add_random_item(hero.random_position(), random.choice(['sword', 'armor', 'shield']), None, None)
Engine.add_hero(hero.random_position(), 'hero')
Engine.add_hero(hero.random_position(), 'ogre')

Engine.redraw()

game_loop = True
while game_loop:
    x = [(0, 0), (0, 0)]

    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            game_loop = False
        if e.type == pygame.KEYDOWN:

            if e.key == pygame.K_a: x[1] = (-1, 0)
            if e.key == pygame.K_d: x[1] = (1, 0)
            if e.key == pygame.K_w: x[1] = (0, -1)
            if e.key == pygame.K_s: x[1] = (0, 1)

            if e.key == pygame.K_LEFT:  x[0] = (-1, 0)
            if e.key == pygame.K_RIGHT: x[0] = (1, 0)
            if e.key == pygame.K_UP: x[0] = (0, -1)
            if e.key == pygame.K_DOWN: x[0] = (0, 1)

            if e.key == pygame.K_q: Engine.heroes[0].hp -= 5
            if e.key == pygame.K_e:
                Engine.heroes[0].revive()
                Engine.heroes[0].image.reload()

    for num, speed in enumerate(x):
        if speed != (0, 0):
            Engine.make_move(Engine.heroes[num], speed)
    Engine.redraw()
    pygame.display.flip()
    clock.tick(60)
