'''
2.Работа с объектом image

[темы]
загрузка изображения
перерисовка только изменяемой части экрана
радиус круга вокруг изображения (гипотенуза)

[задания]:
сделать, чтобы наша картинка двигалась в синем круге с белой границей
'''
import pygame
import math
import os

# начальная инициализация
pygame.init()
# создание часов, отслеживающих частоту отрисовки
clock = pygame.time.Clock()
# загрузка изображения на уровне вверх из папки resources
image = pygame.image.load(os.path.dirname(os.getcwd())+'\\resources\\mario256.png')
# получение размеров изображения в объект Rect
image_rect = image.get_rect()

gip = math.sqrt(image_rect.width // 2 * image_rect.width // 2 + image_rect.height // 2 * image_rect.height // 2)

# используемые переменные
# цвета RGB
black = (0, 0, 0)
white = (255, 255, 255)
# высота и ширина экрана в пикселях
width = 640
height = 480
coord = width // 2, height // 2
image_rect.center = coord
# установка разрешения экрана в пикселях, получение рабочей поверхности Surface
screen = pygame.display.set_mode((width, height))
# прячем курсор мыши
pygame.mouse.set_visible(False)
# закрашиваем всю рабочую поверхность черным
screen.fill(black)

# начало игрового цикла
game_loop = True
while game_loop:
    # получение всех событий, выбор нужных
    for e in pygame.event.get():
        # события выхода (крестик на окне и Escape)
        if e.type == pygame.QUIT: game_loop = False
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_ESCAPE]: game_loop = False

    # закрашиваем предыдущее положение изображения черным
    screen.fill(black, image_rect)

    # закрашиваем предыдущий круг
    pygame.draw.circle(screen, black, image_rect.center, int(gip)+10)

    # получение текущей координаты мышки
    coord = pygame.mouse.get_pos()
    # ставим центр нашего прямоугольника по координате мыши
    image_rect.center = coord

    # рисуем круг вокруг с толщиной 5 пикселей
    pygame.draw.circle(screen, white, image_rect.center, int(gip), 5)

    # выводим наше изображение на основное по координатам image_rect
    screen.blit(image, image_rect)

    # установка задержки отрисовки (максимальный FPS)
    clock.tick(60)
    # обновление изображения на экране
    pygame.display.flip()
