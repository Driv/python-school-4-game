from options import *
import random


def random_position():
    return random.randint(0, map_width - 1), random.randint(0, map_height - 1)


class GameObject:
    def __init__(self, _position, _tile_name):
        self.position = _position
        self.tile_name = _tile_name
        self.image = images.subsurface(tiles[_tile_name][0] * sprite_x, tiles[_tile_name][1] * sprite_y, sprite_x,
                                       sprite_y)

    def draw(self, screen):
        screen.blit(self.image, (self.position[0] * sprite_x, self.position[1] * sprite_y, sprite_x, sprite_y))

    def get_image(self):
        return self.image

    def reload_image(self):
        self.image = images.subsurface(tiles[self.tile_name][0] * sprite_x, tiles[self.tile_name][1] * sprite_y,
                                       sprite_x,
                                       sprite_y)
        return self.image


class Terrain(GameObject):
    object_type = 'terrain'


class Item(GameObject):
    object_type = 'item'
    item_type = None
    item_stats = (0, 0, 0, 0)

    '''
    get random rarity item. 
    type and lvl may be None - so random too  
    '''

    def get_random_item(self, _item_lvl, _item_type):
        self.item_type = _item_type if _item_type else None
        self.item_stats = random.choice(list(
            filter(lambda x: x[2] == _item_lvl if _item_lvl else random.randint(1, max_item_lvl),
                   items[_item_type if _item_type else random.choice(list(items))])))
        return self.item_stats

    '''
    get item of type.
    if rarity exists, then item of this rarity, else random rarity from lvl
    '''

    def get_item(self, _item_lvl, _item_type, _item_rarity):
        self.item_type = _item_type if _item_type else None
        # self.item_stats = filter(lambda x: x[2] == _item_lvl and x[3] == _item_rarity, items[_item_type])
        if _item_rarity:
            self.item_stats = random.choice(list(filter(lambda x: x[3] == _item_rarity, items[_item_type])))
        else:
            self.item_stats = random.choice(list(filter(lambda x: x[2] == _item_lvl, items[_item_type])))
        return self.item_stats

    def get_stats(self):
        return self.item_stats

    def get_attack(self):
        return self.item_stats[0]

    def get_defence(self):
        return self.item_stats[1]

    def get_lvl(self):
        return self.item_stats[2]

    def get_rarity(self):
        return self.item_stats[3]


class Unit(GameObject):
    hp = hp_max = 0
    attack = 0
    defence = 0

    # draw hp bar, attack and defence
    def draw(self, _screen):
        self.reload_image()
        # draw hp bar
        pygame.draw.line(self.image, red, (sprite_x - hp_bar_size, 0), (sprite_x - hp_bar_size, sprite_y), hp_bar_size)
        pygame.draw.line(self.image, green, (
            sprite_x - hp_bar_size, int(sprite_y - self.hp / self.hp_max * sprite_y)),
                         (sprite_x - hp_bar_size, sprite_y),
                         hp_bar_size)

        # draw attack and defence via font
        pygame.draw.line(self.image, filler, (sprite_x - hp_bar_size * 2, 0),
                         (sprite_x - hp_bar_size * 2, sprite_y), hp_bar_size)
        pygame.draw.line(self.image, yellow, (sprite_x - hp_bar_size * 2, 0),
                         (sprite_x - hp_bar_size * 2, self.get_attack()), hp_bar_size)
        pygame.draw.line(self.image, blue, (sprite_x - hp_bar_size * 2, sprite_y),
                         (sprite_x - hp_bar_size * 2, sprite_y - self.get_defence()), hp_bar_size)

        super().draw(_screen)

    def alive(self):
        return self.hp > 0

    def get_defence(self):
        return self.defence

    def get_attack(self):
        return self.attack

    def die(self):
        pass

    def dead(self):
        return self.hp <= 0

    def receive_damage(self, _target):
        self.hp -= max(_target.get_attack() - self.get_defence(), 1)
        if self.hp <= 0:
            self.die()
        return max(_target.get_attack() - self.get_defence(), 1)

    def fight(self, _target):
        if isinstance(_target, Unit):
            # print('Damage to {}:{}'.format(_target.tile_name, _target.receive_damage(self)))
            # print('Damage to {}:{}'.format(self.tile_name, self.receive_damage(_target)))
            _target.receive_damage(self)
            self.receive_damage(_target)


class Hero(Unit):
    object_type = 'hero'
    hp_max = hp = 20
    attack = 2
    lvl = 1
    exp = 0

    def __init__(self, *args):
        super().__init__(*args)
        self.inventory = []

    def get_defence(self):
        return self.defence + sum(i.get_stats()[1] for i in self.inventory)

    def get_attack(self):
        return self.attack + sum(i.get_stats()[0] for i in self.inventory)

    def revive(self):
        self.hp = self.hp_max
        self.inventory[:] = []
        self.position = random.randint(0, map_width - 1), random.randint(0, map_height - 1)


class GameEngine:
    def __init__(self, _screen):
        self.maps = []
        self.items = []
        self.monsters = []
        self.heroes = []
        self.screen = _screen

    def add_terrain(self, _position, _tile_name):
        self.maps.append(Terrain(_position, _tile_name))

    def add_random_item(self, _position, _tile_name, _item_lvl=None, _item_type=None):
        i = Item(_position, _tile_name)
        i.get_random_item(_item_lvl, _item_type)
        self.items.append(i)

    def add_item(self, _position, _tile_name, _item_lvl, _item_type, _item_rarity=None):
        i = Item(_position, _tile_name)
        i.get_item(_item_lvl, _item_type, _item_rarity)
        self.items.append(i)

    def add_hero(self, _position, _tile_name):
        self.heroes.append(Hero(_position, _tile_name))

    def get_objects(self, _position):
        o = []
        o.extend(list(x for x in self.maps if x.position == _position))
        o.extend(list(x for x in self.items if x.position == _position))
        o.extend(list(x for x in self.monsters if x.position == _position))
        o.extend(list(x for x in self.heroes if x.position == _position))
        return o

    def make_move(self, _target, _speed):
        can_move = False
        if not isinstance(_target, Hero): return
        old_position = _target.position
        new_position = _target.position[0] + _speed[0], _target.position[1] + _speed[1]
        if not ((0 <= new_position[0] <= map_width) and (0 <= new_position[1] <= map_height)): return
        for o in self.get_objects(new_position):
            if o.object_type == 'terrain':
                can_move = True
            if o.object_type == 'item':
                _target.inventory.append(o)
                self.items.remove(o)
                can_move = True
            if o.object_type in ['monster', 'hero']:
                can_move = False
                _target.fight(o)
                if _target.dead():
                    _target.revive()
                    _target.reload_image()
                    _target.draw(self.screen)

                if o.dead():
                    o.revive()
                    o.reload_image()
                    o.draw(self.screen)

        if can_move:
            _target.position = new_position

        # redraw both positions
        for o in self.get_objects(old_position):
            o.draw(self.screen)
        for o in self.get_objects(new_position):
            o.draw(self.screen)

    def redraw(self):
        for o in self.maps:
            o.draw(self.screen)
        for i in self.items:
            i.draw(self.screen)
        for h in self.heroes:
            h.draw(self.screen)
