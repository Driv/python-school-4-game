import pygame

map_size = map_width, map_height = 15, 15
sprite_size = sprite_x, sprite_y = 32, 32
screen_size = screen_width, screen_height = map_width * sprite_x, map_height * sprite_y
item_count = int((map_width+map_height)/3.5)
max_item_lvl = 3
red = (255, 0, 0)
green = (0, 255, 0)
yellow = (223, 229, 57)
blue = (65, 83, 219)
filler = (190, 114, 60)
hp_bar_size = 3

pygame.init()
images = pygame.image.load('resources\\tileset.png')
object_types = ['terrain', 'item', 'monster', 'hero']
tiles = {
    'grass': (0, 15),
    'rock': (0, 14),
    'sand': (7, 15),
    'water': (19, 19),
    'hero': (4, 2),
    'ogre': (1, 2),
    'armor': (8, 21),
    'sword': (11, 29),
    'shield': (54, 22)
}

# attack defence lvl rarity
items = {
    'shield': [
        (0, 2, 1, 1),
        (0, 3, 1, 2),
        (0, 4, 1, 3),
        (0, 5, 1, 4),
        (0, 6, 2, 5),
        (0, 7, 2, 6),
        (0, 8, 2, 7),
        (0, 9, 2, 8),
        (0, 11, 3, 9),
        (0, 13, 3, 10)
    ],
    'sword': [
        (4, 0, 1, 1),
        (5, 0, 1, 2),
        (6, 0, 1, 3),
        (7, 0, 1, 4),
        (8, 0, 2, 5),
        (9, 0, 2, 6),
        (10, 0, 2, 7),
        (11, 0, 2, 8),
        (13, 0, 3, 9),
        (15, 0, 3, 10)
    ],
    'armor': [
        (0, 2, 1, 1),
        (0, 3, 1, 2),
        (0, 4, 1, 3),
        (0, 5, 1, 4),
        (0, 6, 2, 5),
        (0, 7, 2, 6),
        (0, 8, 2, 7),
        (0, 9, 2, 8),
        (0, 11, 3, 9),
        (0, 13, 3, 10)
    ]
}
