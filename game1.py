from options import *
import random

speed = [0, 0]
black = 0, 0, 0

clock = pygame.time.Clock()


def get_tile(images, x, y):
    return images.subsurface(x * sprite_x, y * sprite_y, sprite_x, sprite_y)


def valid_move(object, speed):
    res = True
    if (object.left + speed[0] > screen_width) or (object.left + speed[0] < 0): res = False
    if (object.right + speed[0] > screen_width) or (object.right + speed[0] < 0): res = False
    if (object.top + speed[1] > screen_height) or (object.top + speed[1] < 0): res = False
    if (object.bottom + speed[1] > screen_height) or (object.bottom + speed[1] < 0): res = False
    return res


pygame.init()
screen = pygame.display.set_mode(screen_size)
sprites = pygame.image.load('resources\\tileset.png')
ball = get_tile(sprites, 6, 2)
grasses = [get_tile(sprites, i, 15) for i in range(1, 7)]
ballrect = ball.get_rect()
old_ballrect = ball.get_rect()
for x in range(0, screen_width // sprite_x):
    for y in range(0, screen_height // sprite_y):
        screen.blit(random.choice(grasses), pygame.Rect(x * sprite_x, y * sprite_y, sprite_x, sprite_y))
screen.blit(ball, ballrect)
pygame.display.flip()

game_cycle = True
while game_cycle:
    for e in pygame.event.get():
        if e.type == pygame.QUIT: game_cycle = False
        if e.type == pygame.KEYDOWN and e.key == pygame.K_LEFT:
            speed[0] = -sprite_x
        if e.type == pygame.KEYDOWN and e.key == pygame.K_RIGHT:
            speed[0] = sprite_x
        if e.type == pygame.KEYDOWN and e.key == pygame.K_UP:
            speed[1] = -sprite_y
        if e.type == pygame.KEYDOWN and e.key == pygame.K_DOWN:
            speed[1] = sprite_y

    if not (speed[0] == 0 and speed[1] == 0) and valid_move(ballrect, speed):
        old_ballrect = ballrect
        ballrect = ballrect.move(speed)
        speed = [0, 0]
        screen.blit(ball, ballrect)
        screen.blit(random.choice(grasses), old_ballrect)
        pygame.display.flip()
    #pygame.display.update([ballrect, old_ballrect])
    clock.tick(60)
