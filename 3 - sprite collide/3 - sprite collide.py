'''
3. Спрайты и пересечения групп

[темы]
создание спрайтов, использование наследования классов
добавления спрайтов в группы
проверка на пересечение элементов групп спрайтов: попиксельная и круговая
удаление из групп


[задания]:
сделать, чтобы монетки и бомбы не выходили за пределы экрана при создании
сделать, чтобы монетки и бомбы не могли при создании перекрывать друг друга и марио
сделать разное значение монеток
'''
import pygame
import random
import os


class Block(pygame.sprite.Sprite):
    def __init__(self, _screen, _picture, _pos, _group):
        pygame.sprite.Sprite.__init__(self, _group)
        self.image = pygame.image.load(_picture)
        self.rect = self.image.get_rect()
        self.rect.center = _pos
        self.screen = _screen
        # для кругового сравнения пересечений спрайтов
        # т.к. у нас круглые картинки, то пусть радиус будет равен половине ширины
        self.radius = self.rect.width // 2

        # создаем маску для попиксельной проверки пересечений
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        # self.screen.fill(black, self.rect)
        self.screen.blit(self.image, self.rect)

    def clear(self):
        self.screen.fill(black, self.rect)

    def move(self, _pos):
        self.clear()
        self.rect.center = _pos

        # self.__update__(self)


# начальная инициализация
pygame.init()
# создание часов, отслеживающих частоту отрисовки
clock = pygame.time.Clock()

coins_collected = 0

black = (0, 0, 0)
# высота и ширина экрана в пикселях
width = 800
height = 600

# центр экрана
coord = width // 2, height // 2
# установка разрешения экрана в пикселях, получение рабочей поверхности Surface
screen = pygame.display.set_mode((width, height))
# прячем курсор мыши
pygame.mouse.set_visible(False)
# закрашиваем всю рабочую поверхность черным
screen.fill(black)

# создаем группы спрайтов
heroes = pygame.sprite.Group()
coins = pygame.sprite.Group()
bombs = pygame.sprite.Group()

# создаем марио - экземпляр класса Block, добавляем его в группу heroes
Block(screen, os.path.dirname(os.getcwd()) + '\\resources\\mario128.png', coord, heroes)

# делаем от 10 до 15 монеток случайного значения на случайных местах экрана
for i in range(1, random.randint(10, 16)):
    Block(screen, os.path.dirname(os.getcwd()) + '\\resources\\coin' + str(random.choice(['32', '48', '64'])) + '.png',
          (random.randint(0, width), random.randint(0, height)), coins)

# делаем от 5 до 7 бомб на случайных местах экрана
for i in range(1, random.randint(5, 8)):
    Block(screen,
          os.path.dirname(os.getcwd()) + '\\resources\\bomb64.png',
          (random.randint(0, width), random.randint(0, height)), bombs)

# начало игрового цикла
game_loop = True
while game_loop:
    # получение всех событий, выбор нужных
    for e in pygame.event.get():
        # события выхода (крестик на окне и Escape)
        if e.type == pygame.QUIT: game_loop = False
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_ESCAPE]: game_loop = False

    # получение текущей координаты мышки
    coord = pygame.mouse.get_pos()
    # двигаем марио
    for i in heroes:
        i.move(coord)
        # проверяем пересечения с монетками и бомбами
        for c in coins:
            # если подобрали монетку
            if pygame.sprite.collide_circle(i, c):
                coins_collected += 1
                # убираем из всех групп
                c.kill()
                # закрашиваем место монетки
                c.clear()

        for b in bombs:
            # если подобрали бомбу
            if pygame.sprite.collide_circle(i, b):
                # убираем бомбу из всех групп
                b.kill()
                # убираем марио из всех групп
                i.kill()
                # закрашиваем место бомбы и марио
                b.clear()
                i.clear()
                # выходим из игры
                game_loop = False
                break
    # перерисовка всех групп
    heroes.update()
    coins.update()
    bombs.update()

    pygame.display.set_caption('Собрано монеток: {}'.format(coins_collected))

    # установка задержки отрисовки (максимальный FPS)
    clock.tick(60)
    # обновление изображения на экране
    pygame.display.flip()
